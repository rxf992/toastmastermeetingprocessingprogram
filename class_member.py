# -*- coding : utf-8 -*-
__author__ = 'RogerRao'

"""
Officer Roles:
President
vicePresident
SAA(SergeantAtArms)
VPE(VicePresidentEducation)
VPM(VicePresidentMembership)
VPPR(VicePresidentPublicRelationship)
Treasure
Secretary
...

Toast Master Meeting Roles:
Toastmaster
Timer
Ah-counter
Grammarian
Table Topic Master
Table Topic Speaker
Table Topic Evaluator
Prepared Speaker
Individual Evaluator
General Evaluator
...

Award:
BestEvaluator
BestPreparedSpeaker
BestTableTopicSpeaker

"""

"""
class member contains information that can be used to
describe a person who involved in our english club.
"""
class Member:
    memberCount = 0 # static class member, share by all objects.

    # define and calculate member's credit.
    credit_rule = {"Toastmaster": 2,
                   "General Evaluator": 2,
                   "Table Topic Master": 2,
                   "Table Topic Speaker": 1,
                   "Table Topic Evaluator": 1,
                   "Individual Evaluator": 1,
                   "Grammarian": 1,
                   "Timer": 1,
                   "Ah-counter": 1,
                   "Best Evaluator": 1,
                   "Best Prepared Speaker": 1,
                   "Best Table Topic Speaker": 1,
                   "Prepared Speaker": 0}

    def calculate_credit(self):
        for role in self.meeting_roles:
            self.credit += self.meeting_roles[role] * self.credit_rule[role]
        for award in self.meeting_awards:
            self.credit += self.meeting_awards[award] * self.credit_rule[award]


    def __init__(self, name, email="", current_position=""): # default initializer.
        Member.memberCount += 1
        self.name = name
        self.email = email
        self.curOfficerRole = current_position

        ## Current Toastmaster Level : CC1 ~ CC10
        self.CCLevel = "CC0"
        self.credit = 0
        self.meeting_roles = {"Toastmaster": 0, "Timer": 0, "Ah-counter": 0, "Grammarian": 0,
                              "Table Topic Master": 0, "Table Topic Speaker": 0,
                              "Table Topic Evaluator": 0, "Prepared Speaker": 0,
                              "Individual Evaluator": 0, "General Evaluator": 0}

        self.officer_roles = {"President": 0, "vicePresident": 0,"SAA": 0,
                              "VPE": 0, "VPM": 0,"VPPR": 0, "Treasure": 0, "Secretary": 0}
        ### Meeting Awards ###
        self.meeting_awards = {"Best Evaluator": 0,
                               "Best Prepared Speaker": 0,
                               "Best Table Topic Speaker": 0}





