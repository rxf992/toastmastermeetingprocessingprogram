# -*- coding : utf-8 -*-

每次参加Meeting，按照角色不同有不同的积分方法

As a member of Evaluator Team
----------------
1.Toastmaster /  Table topic Master / General Evaluator 积2分
2.Timer / Ah-counter / Grammarian / Individual Evaluator/Table Topic Evaluator积一分。
3.Best Evaluator 再积一分

As a Speaker
----------------
1.Table Topic 参与积一分　，Best Table Topic Speaker 再积一分
2.Prepare Speech 作为晋级测试，本身不积分只晋级。
  注：由会员自己申请是否作为晋级测试，各位Member投标表决 是否完成了该level的蜕变，半数以上通过。　
3.Best Prepare Speaker 积一分，Prepared Speech 本身不积分只晋级。
CC1为Member初始具有认证的资格。其余等级积分逢5的倍数具有参加该级别认证的资格。
举例：参加CC2晋级认证，分数需要达到　5*(2-1)＝5分以上。　参加CC4认证　需要达到5*(4-1)＝15分以上.


附：CC1-10 目标
--------------------
CC1 ：The Ice Breaker
CC2：Organize Your Speech
CC3：Get to The Point
CC4: How to Say it
CC5: Your Body Speaks
CC6: Vocal Variety
CC7: Research Your Topic
CC8: Get Comfortable with Visual Aids
CC9: Persuade with Power
CC10: Inspire Your Audience
