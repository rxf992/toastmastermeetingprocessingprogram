### **About this Project** ###
This project aims to provide automatic statistics of membership activities
via processing Toastmaster Meeting's Agenda files.

The Python Script will find all member's activities by reading the agenda files.
All member's name should be put in a specified member info file.

### **Before Running the Program** ###
* Limitation: this project only supports .xlsx files(Excel 2007 or newer)
* Make sure you have prepared the member excel file and the agenda files correctly.
* The Root Dir Looks like this:
   +[**root_dir**]
    * agenda_files\
    * - agenda_20140910.xlsx
    * - agenda_20140830.xlsx
    * - ......
    * member_info.xlsx
    * main.exe
*  
* Please CLOSE ALL Excel Files First, otherwise the program can not open them and throw exceptions.

### **Running the Program** ###
* The released .exe program will be automatically unpacked to a tmp directory before running.
  Therefore, it is a MUST to Specify the Root DIR Where the Excel files have been put to.
  If Not, the program will throws exceptions.
* [**Usage**]: main.exe <root_dir> <member_file_name> <result_file_name>
* [Important Note]:
  - root_dir: SHOULD BE followed with a **'\'** at the End of it.
  - member_file_name: should NOT contain dir path.
  - result_file_name: should NOT contain dir path.
* [**Example**]:
  - D:\>main.exe D:\Toastmaster_python\ member_info.xlsx test_result.xlsx
 - [Explanation] The program will:
   - Read From
     - D:\Toastmaster_python\member_info.xlsx
     - D:\Toastmaster_python\agenda_files\\*.xlsx
   - Save Result to
     - D:\Toastmaster_python\result.xlsx

### **Want to Improve it?** ###
* Setup the python dev environment by :
    * Install Python 2.7
    * Install openpyxl lib
      https://pythonhosted.org/openpyxl/#installation
    * Common Install Error: http://webscrapy.blog.51cto.com/8343567/1339637
* run cmd:
  #python main.py <root_dir> <member_file_name> <result_file_name>

### **How to Release as an Executable File** ###
* We Use pyInstaller Tool to do this job, so please help yourself to install it correctly.
* **Example**:
  - pyinstaller main.py -F --upx-dir=d:\upx391w

### **Want to Get Help?** ###
please Contact raoxuefeng@yeah.net

-------------------------------
VIA English Club @2014, All Rights Reserved.