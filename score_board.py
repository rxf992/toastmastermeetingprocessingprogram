# -*- coding: utf-8 -*-
__author__ = 'Roger Rao'

import os
import sys
import fnmatch
import re
from openpyxl import Workbook
from openpyxl import load_workbook
#from openpyxl.compat import range
from openpyxl.cell import get_column_letter
from class_member import Member

class ScoreBoard:
    #static var
    numScoreboard = 0

    def print_header(self, header_row=2, header_col=1):
        header = ["Member Name", "CC Level", "Total Score"]
        member = self.members[0]
        for role in member.meeting_roles:
            header.append(role)
        for awards in member.meeting_awards:
            header.append(awards)
        #print header
        for head in header:
            self.ws.cell(row=header_row, column=header_col).value = head
            header_col += 1

    def print_contents(self, row=3, col=1):
        row_idx = row
        col_idx = col
        for member in self.members: # print contents row by row.

            # print name.
            self.ws.cell(row=row_idx, column=col_idx).value = member.name
            # print CC Level.
            col_idx += 1
            self.ws.cell(row=row_idx, column=col_idx).value = member.CCLevel
            # print credit (Total Score).
            # need to calculate the credit first.
            member.calculate_credit()
            col_idx += 1
            self.ws.cell(row=row_idx, column=col_idx).value = member.credit

            # print how many time did this member take part in each role.
            for role in member.meeting_roles:
                times = member.meeting_roles[role]
                col_idx += 1
                self.ws.cell(row=row_idx, column=col_idx).value = times

            for award in member.meeting_awards:
                times = member.meeting_awards[award]
                col_idx += 1
                self.ws.cell(row=row_idx, column=col_idx).value = times

            row_idx += 1  # move to next row.
            col_idx = col  # reset to first column.

        #print "print_contents finish."



    def __init__(self, member_objects=None):
        ScoreBoard.numScoreboard += 1
        #print "score board initialized %d times" % ScoreBoard.numScoreboard
        self.members = member_objects
        self.wb = Workbook()
        self.ws = self.wb.active
        self.ws.title = "Member Score History"

    def read_from_file(self, src_file_name):
        print "# not implemented yet: read from %s" % src_file_name

    def save_to_file(self, dest_filename="summary.xlsx"):
        #print "save_to_file: dest_filename: %s" % dest_filename

#        for col_idx in range(1, 40):
#            col = get_column_letter(col_idx)
#            for row in range(1, 600):
#                ws.cell('%s%s'%(col, row)).value = '%s%s' % (col, row)

        # print Header
        self.print_header(2, 1)   # print from row 2,column 1
        self.print_contents(3, 1) # print contents.
        #
        self.wb.save(filename=dest_filename)

    def print_total_score_by_name(self):
        print "# not implemented yet: print_score_by_name"

    def append_and_update(self):
        print "# not implemented yet: append_and_update"

