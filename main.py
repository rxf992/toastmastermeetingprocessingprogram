# -*- coding : utf-8 -*-
import os
import sys
import fnmatch
from openpyxl import Workbook
from openpyxl import load_workbook
from class_member import Member
from class_agenda import Agenda
from score_board import ScoreBoard

class updater:
    ####this class is used to do all the update job.##

    @staticmethod
    def load_all_agenda_files(dir_name):
        agenda_objects = []
        agenda_list = updater.find_all_agenda_files(dir_name)
        print "load_all_agenda_files:"
        print agenda_list
        for agenda_file in agenda_list:
            agenda_objects.append(Agenda(agenda_file))
        #print agenda_objects
        return agenda_objects

    @staticmethod
    def update_from_agenda(agenda_file_name):
        ## this method will read a agenda file and find out what
        ## the role this member had participated in.
        agenda = load_workbook(filename=agenda_file_name, use_iterators=True)
        sheet = agenda.active(0)
        if sheet is None:
            print "Error: opening agenda file %s failed." % agenda_file_name
            exit(-1)

    @staticmethod
    #if str1 in str2 , return True , else False
    def compare_name(str1, str2):
        #print "agenda taker:"
        #print agenda_taker
        if str1 is None or str2 is None:
            return False
        str1 = ''.join(str1.strip().split()).lower()
        str2 = ''.join(str2.strip().split()).lower()
        return str1 in str2

    @staticmethod
    def associate_agenda_roles_with_member_record(roles, meeting_roles):
        find_matched = False
        #print "Enter associate_agenda_roles_with_member_record."
#        print meeting_roles
#        for role in meeting_roles:
#            print role
#            print meeting_roles[role]
        # To see if an element in roles[] a role in meeting_roles{}.
        for role in roles:
            get = meeting_roles.get(role, -1)
            if get == -1:
                # use compare_name() to fix or throw error info
                for predefined_role in meeting_roles:
                    if updater.compare_name(predefined_role, role):
                        #print "find matched predefine role: %s for %s" % (predefined_role, role)
                        meeting_roles[predefined_role] += 1
                        find_matched = True
                        break
                if not find_matched:
                    print "[WARNING] Role:'%s' is not a pre-defined meeting role." % role
            else:
                # if true, just count ++ to this role
                meeting_roles[role] += 1

        #print "Exit associate_agenda_roles_with_member_record."

    @staticmethod
    def associate_agenda_awards_with_member_record(awards, meeting_awards):
        find_matched = False
        #print "Enter associate_agenda_awards_with_member_record."
#        print meeting_roles
#        for role in meeting_roles:
#            print role
#            print meeting_roles[role]
        # To see if an element in roles[] a role in meeting_roles{}.
        for award in awards:
            get = meeting_awards.get(award, -1)
            if get == -1:
                # use compare_name() to fix or throw error info
                for predefined_award in meeting_awards:
                    if updater.compare_name(predefined_award, award):
                        #print "find matched predefine role: %s for %s" % (predefined_role, role)
                        meeting_awards[predefined_award] += 1
                        find_matched = True
                        break
                if not find_matched:
                    print "[WARNING] Award:'%s' is not a pre-defined Meeting Award." % award
            else:
                # if true, just count ++ to this role
                meeting_awards[award] += 1

        #print "Exit associate_agenda_awards_with_member_record."


    @staticmethod
    def updater_main(root_dir="", member_filename="member_info.xlsx",
                     result_filename="summary.xlsx"):
        #load member' name from xlsx file.
        #print root_dir
        member_list = updater.load_member_name_from_file(root_dir + member_filename)
        #print member_list
        print "total member: %d" % Member.memberCount
        # find and read all agenda files to find out what a member has done in the recent days.
        agenda_objects = updater.load_all_agenda_files(root_dir+"agenda_files")
        member_objects = []
        for member_name in member_list:
            #create member object.
            member = Member(member_name)
            # read from all agenda files to find out what did this member do.
            for ag in agenda_objects:
                roles = ag.query_role_from_taker(member_name)
                awards = ag.query_awards_from_winner(member_name)
                if len(roles):
                    # print "%s : %s" % (member_name, roles)
                    # find out the corresponding key_words in Member Object's record dict.
                    updater.associate_agenda_roles_with_member_record(roles, member.meeting_roles)
                if len(awards):
                    # print "%s : %s" % (member_name, awards)
                    # find out the corresponding key_words in Member Object's record dict.
                    updater.associate_agenda_awards_with_member_record(awards, member.meeting_awards)
            # store member objects.
            member_objects.append(member)
        # generate the result file.
#        for member in member_objects:
#            print "dump------>"
#            print member.name
#            print member.meeting_roles
#            print "<---------dump"
        score_file = ScoreBoard(member_objects)
        score_file.save_to_file(root_dir + result_filename)

        #print "update_main finished."
        print "Credit Result save to File: %s " % (root_dir + result_filename)

    @staticmethod
    def find_all_agenda_files(dir_name):
        agenda_list = []
        file_with_dir = ""
        for ag_file in os.listdir(dir_name):
            if fnmatch.fnmatch(ag_file, "*.xlsx"):
                #print "find agenda file: %s" % ag_file
                file_with_dir = dir_name + "/" + ag_file
                #print file_with_dir
                agenda_list.append(file_with_dir)
        return agenda_list

    @staticmethod
    def load_member_name_from_file(member_file):
        member_list = []
        wb = load_workbook(filename=member_file)
        ws = wb.active
        if ws is None:
            print "Error: open %s failed" % member_file
            exit(-1)
        # find all member's name.
        for row_index in range(1, ws.get_highest_row()+1):
            for column_index in range(1, ws.get_highest_column()+1):
                cell = ws.cell(row=row_index, column=column_index)
                if cell.value == "Name":
                    #print "The Name Cell is at Row %d and Column %s" % (cell.row, cell.column)
                    name_row = cell.row
                    name_column = ord(cell.column) - ord("A") + 1
                    #print "name_column is %d" % name_column
        for row_index in range(name_row+1, ws.get_highest_row()+1):
            cell = ws.cell(row=row_index, column=name_column)
            if cell and cell.value != "":
                member_list.append(cell.value)
                Member.memberCount += 1
                #print "add member : %s email: %s" % (cell.value, ws.cell(row=row_index, column=name_column+1).value)

        #print member_list
        return member_list


def main(argv):
#    for arg in argv:
#        print arg

    if len(argv) < 4:
        print "Need to Specify Root DIR and File Name."
        print "Usage: %s <root_dir> <member_file> <score_file>" % sys.argv[0]
        print "Example: %s D:\\Toastmaster_python\\ member_info.xlsx test_result.xlsx" % sys.argv[0]

        root_dir = raw_input("Enter the Root DIR of all Toastmaster excel files: ")
        if root_dir[-1] != '\\' and root_dir[-1] != '/':
            root_dir += "\\"
        print root_dir

        member_file_name = raw_input("Enter the member excel file name: ")
        index = member_file_name.rfind('.')
        if index == -1:
            # only file name , no .xlsx
            member_file_name += ".xlsx"
        print member_file_name
        if not os.path.exists(root_dir+member_file_name):
            print "Err: member file %s not exists." % (root_dir+member_file_name)

        result_file_name = raw_input("Enter the result excel file name: ")
        index = result_file_name.rfind('.')
        if index == -1:
            # only file name , no .xlsx
            result_file_name += ".xlsx"
        print result_file_name
    else:
        root_dir = sys.argv[1]
        member_file_name = sys.argv[2]
        result_file_name = sys.argv[3]

    updater.updater_main(root_dir, member_file_name, result_file_name)
    os.system("pause")

if __name__ == '__main__':
    main(sys.argv)

__author__ = 'RogerRao'
