# -*- coding : utf-8 -*-
__author__ = 'RogerRao'
import os
import sys
import fnmatch
import re
from openpyxl import Workbook
from openpyxl import load_workbook
from class_member import Member
"""
Class Agenda provides description to ToastMaster Agenda xlsx file.
You can query some info by accessing to this class object.
"""


class Agenda:
    agendaCount = 0     # static var shared by all objects.
    def __init__(self, file_name):
        #print "Agenda Init Function."
        self.fileName = file_name
        # the "Role" Row and Column
        self.role_row = 0
        self.role_column = 0
        # the "Taker" Row and Column
        self.taker_row = 0
        self.taker_column = 0
        # the "Award" Row and Column
        self.award_row = 0
        self.award_column = 0
        # the "Winner" Row and Column
        self.winner_row = 0
        self.winner_column = 0

        ### load agenda file.
        if None is re.search(r'.*.xlsx', self.fileName, re.I):
            print "Agenda File is not a xlsx file. Exiting."
            exit(-1)
        self.wb = load_workbook(filename=self.fileName)
        self.ws = self.wb.active
        if self.ws is None:
            print "Error: opening agenda file %s failed." % self.fileName
            exit(-1)

        ##find Column "Role"
        for row_index in range(1, self.ws.get_highest_row()+1):
            for column_index in range(1, self.ws.get_highest_column()+1):
                role = self.ws.cell(row=row_index, column=column_index)
                if role.value == "Role":
                    taker = self.ws.cell(row=row_index, column=column_index + 1)
                    if taker.value == "Taker":             # find Taker
                        self.role_row = role.row
                        self.role_column = ord(role.column) - ord("A") + 1
                        self.taker_row = self.role_row
                        self.taker_column = self.role_column + 1
                        #print "Taker find !!!"
                    else:
                        print "[ERROR] Can not find Column ""Taker"", Please Check the File Content."
                    break

        ##find Column "Award"
        print "try to find Award Column"
        for row_index in range(1, self.ws.get_highest_row()+1):
            for column_index in range(1, self.ws.get_highest_column()+1):
                award = self.ws.cell(row=row_index, column=column_index)
                if award.value == "Award":
                    winner = self.ws.cell(row=row_index, column=column_index + 1)
                    if winner.value == "Winner":
                        self.award_row = award.row
                        self.award_column = ord(award.column) - ord("A") + 1
                        self.winner_row = self.award_row
                        self.winner_column = self.award_column + 1
                        #print "Winner find !!!"
                    else:
                        print "[ERROR] Can not find Column ""Winner"" in %s," \
                              "Please Check the File Content. " % self.fileName
                    break

        ### TODO: Fix this:
        ## currently Table Topic Speaker are put together with Award and Winners' Content.
        ## if the Winner Column is not the same as Taker, the program will not find Table Topic Speaker.
        if self.winner_column != self.taker_column:
            print "[WARNING] the Winner Column should be the same as Taker"
        if self.award_column != self.role_column:
            print "[WARNING] the Award Column should be the same as Role"

    def compare_name(self, member_name, agenda_taker):
        #print "agenda taker:"
        #print agenda_taker
        if member_name is None or agenda_taker is None:
            return False
        str1 = member_name
        str2 = agenda_taker
        str1 = ''.join(str1.strip().split()).lower()
        str2 = ''.join(str2.strip().split()).lower()
        return str1 in str2

    def query_role_from_taker(self, taker_name):
        roles = []  # a person could have more than one role.
        #find the taker and its roles.
        ##find Column "Role"
        for row_index in range(self.taker_row+1, self.ws.get_highest_row()+1):
            taker = self.ws.cell(row=row_index, column=self.taker_column)
            #print "taker:%d : %s" %(row_index - self.taker_row, taker.value)
            if self.compare_name(taker_name, taker.value):
                cell = self.ws.cell(row=row_index, column=self.role_column)
                #print "find taker:%s as role: %s" % (taker_name, cell.value)
                roles.append(cell.value)
        # use 'set' data structure to remove repeated roles.
        roles_set = set(roles)
        return roles_set

    def query_awards_from_winner(self, winner_name):
        if self.winner_column == 0 or self.winner_row == 0:
            print "[ERROR] query_awards_from_winner() : No Valid Record Found" \
                  "in %s . Check your Agenda Files." % self.fileName
            return set([])
        awards = []  # a person could have more than one awards.
        #find the taker and its awards.
        ##find the Cell name as "Awards"
        for row_index in range(self.winner_row+1, self.ws.get_highest_row()+1):
            winner = self.ws.cell(row=row_index, column=self.winner_column)
            #print "query_awards_from_winner: taker:%d : %s" %(row_index - self.taker_row, taker.value)
            if self.compare_name(winner_name, winner.value):
                cell = self.ws.cell(row=row_index, column=self.role_column)
                #print "find taker:%s as role: %s" % (taker_name, cell.value)
                awards.append(cell.value)
        # use 'set' data structure to remove repeated roles.
        awards_set = set(awards)
        return awards_set

    def query_taker_from_role(self, role_name):
        takers = []  # some roles could have more than one taker.
        #TODO
        return takers